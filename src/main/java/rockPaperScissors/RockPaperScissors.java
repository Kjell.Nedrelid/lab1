package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    
    private Boolean exitGame() {
		String answer = readInput("Do you wish to continue playing? (y/n)?");

    	//if (answer.length() > 1) {
		//	return true;
    	//}
		
    	char shortAnswer = answer.toLowerCase().toCharArray()[0];
    	if (shortAnswer == 'n') {
    		return true;
    	}
    	

    	return false;
    }

    
    
    
    private String validRock() {
    	//Array validAnswers = ['rock', 'paper', 'scissors'];
    	
    	String userInput;
    	while (true) {
    		userInput = readInput("Your choice (Rock/Paper/Scissors)?");
    		// convert to lowercase
    		userInput = userInput.toLowerCase();
    		if (rpsChoices.contains(userInput)) {
    			return userInput;
    		}
    		// Didn't find userInput, give error message and try again.
    		System.out.println("I do not understand " + userInput + ". Could you try again?");
    		
    	}
    			
    }
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	//System.out.println("welcome to the rokk paper skissor.");
    	
    	String userInput;
    	String computerGuess;
    	
    	
    	
    	while (true) {
    		System.out.println("Let's play round " + roundCounter);
    		roundCounter ++;
    		
    		// Ask for valid input...
    		userInput = validRock();
    		
    		// Generate computer input.
    		computerGuess = getComputerGuess();
    		
    		// Check winner and print depending on this...
    		// NOTE, it's possible to make this less ugly and less duplication of code.
    		if (isWinner(userInput, computerGuess)) {
    			// user is winner
    			System.out.println("Human chose " + userInput + ", computer chose " + computerGuess + ". Human wins!");
    			humanScore ++;
    		}
    		else if (isWinner(computerGuess, userInput)) {
    			System.out.println("Human chose " + userInput + ", computer chose " + computerGuess + ". Computer wins!");
    			computerScore ++;
    		}
    		else {
    			System.out.println("Human chose " + userInput + ", computer chose " + computerGuess + ". It's a tie!");
    		}
    		
    		// Show current scores.
    		System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    		
    		// Time to exit...
    		if (exitGame()) {
    			break;
    		}
    	// Add extra security to not have infinite loop during testing.
    	//	if (roundCounter > 5) {
    	//		System.out.println("terminated due to extra security.");
    	//		break;
    	//	}
    		
    	}
    	System.out.println("Bye bye :)");
    }

    private String getComputerGuess() {
		// TODO Auto-generated method stub
    	Random ran = new Random();
    	int randomInt = ran.nextInt(0, rpsChoices.size());
    	//System.out.println(randomInt);
    	//String test = 
		return rpsChoices.get(randomInt);
	}


    private boolean isWinner(String var1, String var2) {
    	// basically copy-paste, since no idea WTF is really correct.
    	if (var1 == "paper") {
    		return var2 == "rock";
    	}
    	if (var1 == "scissors") {
    		return var2 == "paper";
    	}
    	return var2 == "scissors";
    }
    


	/**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
